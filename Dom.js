const companyName = document.querySelector('.company-text');
const companySize = document.querySelector('.company-size');
const companyCurSize = document.querySelector('.company-current');
const tbody = document.querySelector('tbody');
const usersNum = document.getElementsByClassName('row-number');

function renderCompany(company) {
  companyName.textContent = company.name;
  companySize.textContent = company.maxSize;
  companyCurSize.textContent = company.curSize;
}  

function renderUser(user) {
  companyCurSize.textContent = `${company.curSize}`;  
  const template = `
  <th scope="row" class="row-number">${company.curSize - 1}</th>
  <td>${user.name}</td>
  <td>${user.lastName}</td>
  <td>${user.role || ""}</td>
  <td>${user.id}</td>
  <td> 
  <button type="button" class="js-btnDelete btn btn-danger .btn-sm" aria-label="Close">X</button>
  </td>`;
  const tableRow = document.createElement('tr');
  tableRow.innerHTML = template;
  tableRow.setAttribute("data-id", `${user.id}`);
  tbody.append(tableRow);
}

function updateUser(data) {
  
  if (typeof data !== "object") {
    const user = document.querySelector(`tr[data-id="${data}"]`);
    user.remove();
    handlePopUpWindow(data, 'deleted');
    let currentNum = 1;
    Array.from(usersNum).forEach((num) => num.textContent=`${currentNum++}`);
    companyCurSize.textContent = `${company.curSize - 1}`; 
    return;
  }
  const updatedUser = document.querySelector(`[data-id="${data.id}"]`);
  companyCurSize.textContent = `${company.curSize}`;
  const curNum = updatedUser.firstElementChild.textContent;
  updatedUser.innerHTML = `
  <th scope="row" class="row-number">${curNum}</th>
  <td>${data.name}</td>
  <td>${data.lastName}</td>
  <td>${data.role || ""}</td>
  <td>${data.id}</td>
  <td><button type="button" class="js-btnDelete btn btn-danger" aria-label="Close">X</button></td>
  `; 
}

function handlePopUpWindow(data, message){
  const popUpMessage = message ? `User ID ${data} has been ${message}` : 'The last place has been taken. No more space left';
  const container = document.querySelector(".toast-container");
  const template = ` 
  <div aria-live="polite" aria-atomic="true" style="position: relative; min-height: 200px; ">
  <div class="toast" style="position: absolute; top: 0; right: 0; ">
    <div class="toast-header" style="background-color: #D9EFE7; padding: 5px">
      <strong class="mr-auto">Notification</strong>
    </div>
    <div class="toast-body" style="background-color: #D9EFE7; padding: 15px 15px;">
      ${popUpMessage}
    </div>
  </div>
</div>`;

Object.assign(container.style,{opacity: 1, zIndex: 1})
  container.innerHTML = template; 
  setTimeout(function() {
    Object.assign(container.style,{opacity: 0, zIndex: -1, transition: "1s"});
  }, 2000);
}

document.body.addEventListener("click", function(event) {
  const target = event.target;
  if (target.classList.contains('js-btnDelete')) {
    const parent = target.closest('tr');
    const userId = parent.getAttribute("data-id");
    admin.deleteUser(userId);
    parent.remove();
    let currentNum = 1;
    Array.from(usersNum).forEach((num) => num.textContent=`${currentNum++}`);
    companyCurSize.textContent = `${company.curSize}`; 
  }
});