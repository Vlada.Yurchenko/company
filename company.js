var Company = (function () {

const token = Symbol("token");
const TOKEN = "secret token";
const userUpdateCallbacks = Symbol("userUpdateCallbacks");
const userCreateCallbaks = Symbol("userCreateCallbaks");
const noSpaceNotifyers = Symbol("noSpaceNotifyers");

 class Company {
    constructor(name, maxSize) {
      this.name = name;
      this.maxSize = maxSize;
      this[userUpdateCallbacks] = [];
      this[userCreateCallbaks] = [];
      this[noSpaceNotifyers] = [];
    }

    #users = [];
    #password;  

    get userList() {
      return this.#users;
    }
    
    get companyPassword() {
      return this.#password;
    }

    set companyPassword(password) {
      this.#password  = password;
    }

    get curSize() {
      return this.userList.length;
    }

    static createSuperAdmin(company) {
      if (company.userList.length) {
        throw new Error ("Admin has already been created!!!");
      }
      let password = prompt("Enter the password");
      let admin = new User("admin", "ADMIN", true);
      admin.company = company;
      company.companyPassword = password;
      company.userList.push(admin);
      return admin;
    }

    getUser(userId) {
      let user = this.userList.find((el) => el.id == userId);
      return user;
    } 

    registerUserCreateCallback(cb) {
      this[userCreateCallbaks].push(cb);
    }

    registerUserUpdateCallback(cb) {
      this[userUpdateCallbacks].push(cb);
    }

    registerNoSpaceNotifyer(cb) {
      this[noSpaceNotifyers].push(cb)
    }

  }
  
  class User {
    constructor(name, lastName, isAdmin){
      this._name = name;
      this._lastName = lastName;
      this.id = + new Date();
      this._role = "";
      if (isAdmin) { 
        this[token] = TOKEN;
        this.createUser = function(name, lastName) {
          superUserVerification.call(this, this[token]);
          let newUser = new User(name, lastName);
          let usersCompany = this.company;
          newUser[userUpdateCallbacks] = usersCompany[userUpdateCallbacks];
          if (usersCompany.userList.length > usersCompany.maxSize - 1) {
            throw new Error("No space!");
          } 
          usersCompany.userList.push(newUser);
          usersCompany[userCreateCallbaks].forEach(el => el(newUser));
          if (usersCompany.userList.length == usersCompany.maxSize) {
            setTimeout(function(){
              usersCompany[noSpaceNotifyers].forEach((el) => el());
            }, 3000);
          } 

          return newUser;         
        } 
        this.deleteUser = function(userId) {
          superUserVerification.call(this, this[token]);
          let users = this.company.userList;
          let userIndextoDelete = users.findIndex((el) => el.id == userId);
          if (userIndextoDelete < 0) {
            throw new Error("No such user!");
          } 
          this.company[userUpdateCallbacks].forEach((el) => el(users[userIndextoDelete].id));
          users.splice(userIndextoDelete, 1);
        } 
      }
    }

    get name() {
      return this._name;
    }

    get lastName() {
      return this._lastName;
    }

    get role() {
      return this._role;
    }

    set name(value) {
      if (!this[userUpdateCallbacks].length) {
        alert(`User ${this.name} has changed the name`);
      }
      this._name = value;
      this[userUpdateCallbacks].forEach((cb) => cb(this));
      
    }

    set lastName(value) {
      if (!this[userUpdateCallbacks].length) {
        alert(`User ${this.name} has changed the lastName`);
      }
      this._lastName = value;
      this[userUpdateCallbacks].forEach((cb) => cb(this));
      
    }

    set role(value) {
      if (!this[userUpdateCallbacks].length) {
        alert(`User ${this.name} has changed the role`);
      }
      this._role = value;
      this[userUpdateCallbacks].forEach((cb) => cb(this));
      
    }

  }

  
function superUserVerification(token) {
  if (token !== TOKEN) {
    throw new Error("Error");
  }
  let checkPassword = prompt("Enter the password");
  if (this.company.companyPassword !== checkPassword) {
    throw new Error("Admin is not authorized");
  }
}

  return Company;
  
})();