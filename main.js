const company = new Company('Yojji', 3);
const admin = Company.createSuperAdmin(company);

renderCompany(company);

company.registerUserCreateCallback(user => {
  renderUser(user);
  handlePopUpWindow(user.id, 'created');
});

company.registerUserUpdateCallback((data) => {
  updateUser(data);
  if (typeof data === "object") {  
  handlePopUpWindow(data.id, 'updated');
  return;
  }
  handlePopUpWindow(data, 'deleted');
});

company.registerNoSpaceNotifyer(handlePopUpWindow);